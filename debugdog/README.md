# DebugDog

#### 介绍
网络通讯测试软件

#### 软件架构
本软件是基于 python 3.8.10 windows 系统制作, 另外需要安装pyserial模块，感谢此软件开发者。通过以下命令安装
```
    pip install pyserial
```


#### 安装教程

1.  安装 python 3.8.10 
    pip install pyserial
    linux下请安装 tkinter 模块
2.  执行 python  debugdog.py

#### 使用说明

1.  debugdog.py 为主程序


#### 参与贡献

