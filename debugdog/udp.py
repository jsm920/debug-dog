﻿import socket , time

# sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM ,socket.IPPROTO_UDP)
# sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1) # 设置广播

# sock.sendto(b"aaaaaaaaaaaaaaaaaaaaaaa", ("224.0.1.0", 8001))

# for i in range(19):
#     sock.sendto(b"11223344556677889900", ("224.0.1.0", 8001))
#     try:
#         len, addr = sock.recvfrom(4096)
#     except Exception as e:
#         print (e)
#     print(len,addr)

def ip_in_range(ip:str, r1:str, r2:str):

    a = ip_to_int(ip)
    b = ip_to_int(r1)
    c = ip_to_int(r2)
    if b > c: 
        t = b
        b = c
        c = t
    return True if a>=b and a < c else False
    def ip_to_int(ip:str):
        try:
            l = [int(c) for c in ip.split(".")]
            return l[0] << 24 | l[1] << 16 | l[2]<< 8 | l[3]
        except:
            return 0xEFFFFFFF
    
print( ip_in_range("192.168.1.11", "192.168.1.12", "192.168.1.10"))