﻿class PData():
    _types:dict = {}
    def __init__(self) -> None:
        self.type = None
        self.info = None
        pass
    
    @staticmethod
    def AddType(t):
        PData._types[t.GetClassName()] = t
        pass
    
    @staticmethod
    def GetTypes():
        return PData._types
        


        